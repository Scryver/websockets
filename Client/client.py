#!/usr/bin/python3

import asyncio
import websockets
import os
import sys


SERVER_PORT = os.environ.get("SERVER_PORT", None)

if SERVER_PORT is None:
    print("SERVER_PORT is None")
    sys.exit(1)

tmp = SERVER_PORT.split(":")

if len(tmp) != 3:
    print("Control port split went wrong, {}".format(tmp))
    sys.exit(1)

config = {
    "host": tmp[1][2:],
    "port": int(tmp[2])
}

@asyncio.coroutine
def hello():
    websocket = yield from websockets.connect('ws://{host}:{port}/'.format(**config))
    name = input("What's your name? ")
    yield from websocket.send(name)
    print("> {}".format(name))
    greeting = yield from websocket.recv()
    print("< {}".format(greeting))


if __name__ == '__main__':
    print("Starting client")
    asyncio.get_event_loop().run_until_complete(hello())