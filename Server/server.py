#!/usr/bin/python3

import asyncio
import websockets
import os

@asyncio.coroutine
def hello(websocket, path):
    name = yield from websocket.recv()
    print("< {}".format(name))
    greeting = "Hello {}!".format(name)
    yield from websocket.send(greeting)
    print("> {}".format(greeting))

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))
    
    start_server = websockets.serve(hello, '0.0.0.0', port)

    print("Starting server")
    asyncio.get_event_loop().run_until_complete(start_server)

    try:
        print("Running server")
        asyncio.get_event_loop().run_forever()
    except:
        pass
    finally:
        asyncio.get_event_loop().close()
